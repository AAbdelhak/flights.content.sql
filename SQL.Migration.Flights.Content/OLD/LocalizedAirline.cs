﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SQL.Migration.Flights.Content.OLD
{
    [Table("LocalizedAirline", Schema = "Localization")]
    public class LocalizedAirline
    {
        public int Id { get; set; }
        [ForeignKey("Airline")]
        public string IATAcode { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public Airline Airline { get; set; }
    }
}