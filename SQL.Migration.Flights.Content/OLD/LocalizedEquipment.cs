﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SQL.Migration.Flights.Content.OLD
{
    [Table("LocalizedEquipment", Schema = "Localization")]
    public class LocalizedEquipment
    {
        public int Id { get; set; }
        [ForeignKey("Equipment")]
        public string IATAcode { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public Equipment Equipment { get; set; }
    }
}