﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace SQL.Migration.Flights.Content.OLD
{
    public class OLD_FlightsContentDbContext : DbContext
    {
        public OLD_FlightsContentDbContext() : base(new DbContextOptionsBuilder<OLD_FlightsContentDbContext>()
            .UseSqlServer("Data Source=35.227.57.224;initial catalog=Flights.Content;user id=LeGateUser;password=LE@dbconnection2015;MultipleActiveResultSets=True;", providerOptions => providerOptions.CommandTimeout(60)).Options)
        {
        }

        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<LocalizedEquipment> LocalizedEquipment { get; set; }

        public DbSet<Airline> Airlines { get; set; }
        public DbSet<LocalizedAirline> LocalizedAirline { get; set; }
    }
}
