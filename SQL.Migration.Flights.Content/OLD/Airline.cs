﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQL.Migration.Flights.Content.OLD
{
    [Table("Airlines", Schema = "dbo")]
    public class Airline
    {
        [Key]
        public string IATAcode { get; set; }
        public string ICAOcode { get; set; }
        public bool IsActive { get; set; }
        public string Phone { get; set; }
        public string AirlineCategoryCode { get; set; }
        public string LogoUrl { get; set; }
        public int Priority { get; set; }
        public ICollection<LocalizedAirline> LocalizedAirlines { get; set; }
    }
}