﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQL.Migration.Flights.Content.OLD
{
    [Table("Equipments", Schema = "dbo")]
    public class Equipment
    {
        [Key]
        public string IATAcode { get; set; }
        public string Name { get; set; }
        public bool TurboPropulsion { get; set; }
        public bool JetPropulsion { get; set; }
        public bool WideBodyAirframe { get; set; }
        public bool RegionalAirframe { get; set; }
        public ICollection<LocalizedEquipment> LocalizedEquipments { get; set; }
    }
}
