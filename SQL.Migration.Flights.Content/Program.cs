﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SQL.Migration.Flights.Content.N;
using SQL.Migration.Flights.Content.OLD;
using LocalizedAirline = SQL.Migration.Flights.Content.N.LocalizedAirline;
using LocalizedEquipment = SQL.Migration.Flights.Content.N.LocalizedEquipment;

namespace SQL.Migration.Flights.Content
{
    class Program
    {
        static void Insert_LocalizedEquipments()
        {
            var newDB = new NEW_FlightsContentDbContext();
            var oldDb = new OLD_FlightsContentDbContext();

            var oldLoc = oldDb.LocalizedEquipment.Include(x => x.Equipment).ToList();
            var newLoc = newDB.LocalizedEquipment.Include(x => x.Equipment).ToDictionary(x => x.Equipment.IATAcode);
            var IATAdic = newDB.Equipment.Include(x => x.LocalizedEquipments).ToDictionary(x => x.IATAcode);

            var listToInsert = new List<LocalizedEquipment>();

            foreach (var rowEquipment in oldLoc)
            {
                if (IATAdic.ContainsKey(rowEquipment.IATAcode))
                {
                    if (!(rowEquipment.Name == newLoc[rowEquipment.IATAcode].Name && rowEquipment.Language == newLoc[rowEquipment.IATAcode].Language))
                    {
                        listToInsert.Add(new LocalizedEquipment()
                        {
                            Language = rowEquipment.Language,
                            Name = rowEquipment.Name,
                            EquipmentId = IATAdic[rowEquipment.IATAcode].Id
                        });
                    }
                }
            }

            newDB.LocalizedEquipment.AddRange(listToInsert);
            Console.WriteLine(newDB.SaveChanges() > 0 ? "inserted" : "error");

        }

        static void RemoveDuplicates_LocalizedEquipments()
        {
            var newDB = new NEW_FlightsContentDbContext();
            var oldDb = new OLD_FlightsContentDbContext();

            var oldLoc = oldDb.LocalizedEquipment.Include(x => x.Equipment).Where(x => x.Language == "en").ToDictionary(x => x.IATAcode);
            var newLoc = newDB.LocalizedEquipment.Include(x => x.Equipment).Where(x => x.Language == "en").ToList();
            var IATAdic = newDB.Equipment.Include(x => x.LocalizedEquipments).ToDictionary(x => x.IATAcode);

            var listToRemove = new List<LocalizedEquipment>();

            foreach (var loc in newLoc)
            {
                if (oldLoc.ContainsKey(loc.Equipment.IATAcode) && loc.Name == oldLoc[loc.Equipment.IATAcode].Name)
                {
                    if (newLoc.Any(x => x.EquipmentId == loc.EquipmentId && x.Id != loc.Id))
                    {
                        listToRemove.Add(loc);
                    }
                }
            }

            newDB.LocalizedEquipment.RemoveRange(listToRemove);
            Console.WriteLine(newDB.SaveChanges() > 0 ? "removed" : "error");
        }

        static void Insert_LocalizedAirline()
        {
            var newDB = new NEW_FlightsContentDbContext();
            var oldDb = new OLD_FlightsContentDbContext();

            var oldLoc = oldDb.LocalizedAirline.Include(x=>x.Airline).ToList();
            var newLoc = newDB.LocalizedAirlines.Include(x => x.Airline).ToDictionary(x => x.Airline.IATAcode);
            var IATAdic = newDB.Airlines.Include(x => x.LocalizedAirlines).ToDictionary(x => x.IATAcode);

            var listToInsert = new List<LocalizedAirline>();

            foreach (var rowEquipment in oldLoc)
            {
                if (IATAdic.ContainsKey(rowEquipment.IATAcode))
                {
                    if (rowEquipment.Language != newLoc[rowEquipment.IATAcode].Language)
                    {
                        listToInsert.Add(new LocalizedAirline()
                        {
                            Language = rowEquipment.Language,
                            Name = rowEquipment.Name,
                            AirlineId = IATAdic[rowEquipment.IATAcode].Id
                        });
                    }
                }
            }

            newDB.LocalizedAirlines.AddRange(listToInsert);
            Console.WriteLine(newDB.SaveChanges() > 0 ? "inserted" : "error");

            //listToInsert.ForEach(x=> Console.WriteLine($"{x.Name},{x.Language} , {x.AirlineId}"));
            //Console.WriteLine(listToInsert.Count(x => x.Language == "ar"));
            //Console.WriteLine(listToInsert.Count(x => x.Language == "fr"));

        }
        static void Main(string[] args)
        {

            Console.ReadLine();
        }
    }
}