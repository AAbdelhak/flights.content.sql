﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace SQL.Migration.Flights.Content.N
{
    public class NEW_FlightsContentDbContext : DbContext
    {
        public NEW_FlightsContentDbContext() : base(new DbContextOptionsBuilder<NEW_FlightsContentDbContext>()
            .UseSqlServer("Data Source=35.227.57.224;initial catalog=Flights.Content.Service;user id=LeGateUser;password=LE@dbconnection2015;MultipleActiveResultSets=True;", providerOptions => providerOptions.CommandTimeout(60)).Options)
        {
        }

        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<LocalizedEquipment> LocalizedEquipment { get; set; }

        public DbSet<Airline> Airlines { get; set; }
        public DbSet<LocalizedAirline> LocalizedAirlines { get; set; }
    }
}
