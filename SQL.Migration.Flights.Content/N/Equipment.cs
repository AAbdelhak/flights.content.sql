﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQL.Migration.Flights.Content.N
{

    [Table("Equipment", Schema = "dbo")]

    public class Equipment
    {
        public int Id { get; set; }
        public string IATAcode { get; set; }
        public bool TurboPropulsion { get; set; }
        public bool JetPropulsion { get; set; }
        public bool WideBodyAirframe { get; set; }
        public bool RegionalAirframe { get; set; }
        public ICollection<LocalizedEquipment> LocalizedEquipments { get; set; }
    }
}
