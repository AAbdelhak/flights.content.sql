﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQL.Migration.Flights.Content.N
{
    [Table("Airline", Schema = "dbo")]
    public class Airline
    {
        public int Id { get; set; }
        public string IATAcode { get; set; }
        public string ICAOcode { get; set; }
        public bool IsActive { get; set; }
        public string Phone { get; set; }
        public int? AirlineCategoryId { get; set; }
        public string LogoUrl { get; set; }
        public int Priority { get; set; }
        public int Rate { get; set; }
        public ICollection<LocalizedAirline> LocalizedAirlines { get; set; }
    }
}