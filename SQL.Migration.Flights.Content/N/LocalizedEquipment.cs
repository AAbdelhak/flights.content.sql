﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SQL.Migration.Flights.Content.N
{
    [Table("LocalizedEquipment", Schema = "Localization")]
    public class LocalizedEquipment
    {
        public int Id { get; set; }
        public int EquipmentId { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public Equipment Equipment { get; set; }
    }
}